<?php

function oc_oauthclient_layout() {
    wp_enqueue_script("serverlist", plugins_url( '/assets/js/serverslist.js', __FILE__ ) );
    wp_enqueue_script("servernames", plugins_url( '/assets/js/select_server.js', __FILE__ ) );
    wp_enqueue_style("css_styles", plugins_url('/assets/css/layout.css', __FILE__));
    
    isset($_GET['tab']) ? $active_tab = sanitize_text_field($_GET['tab']) : $active_tab = 'oauthclientconfig';
?>
    <div class="wrap">
        <h2>WP OAuth Client SSO</h2>
        <h2 class="nav-tab-wrapper" style="border:none;">
            <a href="<?php echo add_query_arg(array('tab' => 'oauthclientconfig'), $_SERVER['REQUEST_URI']); ?>" class="nav-tab <?php echo $active_tab == 'OAuthClientConfig' ? 'nav-tab-active' : ''; ?>" style="border-bottom:1px solid #ccc;">OAuth Client Configuration</a>
            <a href="<?php echo add_query_arg(array('tab' => 'oauthclient_howto'), $_SERVER['REQUEST_URI']); ?>" class="nav-tab <?php echo $active_tab == 'oauthclient_howto' ? 'nav-tab-active' : ''; ?>" style="border-bottom:1px solid #ccc;">How-to</a>
            <a href="<?php echo add_query_arg(array('tab' => 'help'), $_SERVER['REQUEST_URI']); ?>" class="nav-tab <?php echo $active_tab == 'help' ? 'nav-tab-active' : ''; ?>" style="border-bottom:1px solid #ccc;">Help</a>
        </h2>
    </div>

    <!--better CSS purpose like container -->
    <?php
    if ($active_tab === 'oauthclientconfig') oauthclientconfig();
    else if ($active_tab === 'oauthclient_howto') oauthclient_howto();
    else if ($active_tab === 'help') oauth_help();
}

function oauthclientconfig() {
?>
    <div class="card" style="width:50% !important">
        <div>
            <h3> Configuration to OAuth Client </h3>
            <form id="oauthconfig" method="post" action="">
                <input type="hidden" name="action" value="oauthconfig" />
                <?php wp_nonce_field('OAuthConfig_nonce', 'OAuthConfig_nonce') ?>
                <table >
                    <tr>
                        <td>OAuth Server</td>
                        <td id="oc_oauthservernames">
                        </td>
                    </tr>
                    <tr>
                        <td>App Name</td>
                        <td><input type="text" id="app_name"  name="app_name" placeholder="provide suitable name to your app" value="<?php echo esc_attr(get_option('oc_appname')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>CallBack URL/ Redirect URL</td>
                        <td> <input type="text" id="callback_url" name="callback_url" style="width:25rem;" placeholder='.home_url().' value="<?php echo home_url(); ?>" disabled />
                        </td>
                    </tr>
                    <tr>
                        <td>Client ID / Application ID</td>
                        <td> <input type="text" id="client_id" name="client_id" style="width:25rem;" placeholder="Enter the client ID received from the OAuth Server" value="<?php if (get_option('oc_clientid')) echo esc_attr(get_option('oc_clientid')); ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td>Client Secret</td>
                        <td> <input type="text" id="client_secret" name="client_secret" style="width:25rem;" placeholder="Enter the client Secret key received from the OAuth Server" value="<?php if (get_option('oc_clientsecret')) echo esc_attr(get_option('oc_clientsecret')); ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td>Scope</td>
                        <td> <input type="text" id="client_scope" name="client_scope" style="width:25rem;" placeholder="Enter the scope to receive the infromation/data from Server" value="<?php if (get_option('oc_clientscope')) echo esc_attr(get_option('oc_clientscope')); ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td>Authorization Endpoint</td>
                        <td> <input type="text" id="client_authorization" name="client_authorization" style="width:25rem;" placeholder="Enter the Authorization Endpoint URL from the Server" value="<?php if (get_option('oc_client_authorization')) echo esc_attr(get_option('oc_client_authorization')); ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td>Token Endpoint</td>
                        <td> <input type="text" id="client_token_endpoint" name="client_token_endpoint" style="width:25rem;" placeholder="Enter the Token Endpoint URL from the server" value="<?php if (get_option('oc_client_token_endpoint')) echo esc_attr(get_option('oc_client_token_endpoint')); ?>" />
                        </td>
                    </tr>

                    <tr id="oc_userinfo_field" >
                        <td>Userinfo Endpoint</td>
                        <td> <input type="text" id="client_userinfo_endpoint" name="client_userinfo_endpoint" style="width:25rem;" placeholder="Enter the Userinfo Endpoint URL" value="<?php if (get_option('oc_client_userinfo_endpoint')) echo esc_attr(get_option('oc_client_userinfo_endpoint')); ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" style="padding-left: 8rem; padding-top:1rem;"><input class="buttons_style" type="submit" id="clientconfig" value="Save Configuration" /></td>
                    </tr>
                </table>
            </form>
        </div>

        <div style="margin-top:3rem;">
            <h3> Attribute Mapping</h3>
            <p>(You need to fill in the attribute mapping to perform SSO. So the plugin can login/create the user in the wordpress using those attributes)</p>
            <input type="button" class="buttons_style get_attr_button"  value="Get Attributes" <?php if(!get_option('oc_clientsecret')) echo "disabled";?>onclick="getattributes();" />
            <p>To see the which attributes are coming from the oauth server, please click the button and log in with a user. You can change the scope to receive more attributes relatively.</p>

            <form id="attributemapping" method="post" action="">
                <input type="hidden" name="action" value="attributemapping" />
                <?php wp_nonce_field('attributemapping_nonce', 'attributemapping_nonce') ?>
                <table>
                    <tr>
                        <td><label>Username</label></td>
                        <td><input type="text" id="username" name="username" style="width:15rem;" placeholder="Enter the username attribute name received from OAuth Server" value="<?php if (get_option('oc_username')) echo esc_attr(get_option('oc_username')); ?>" />
                    </tr>
                    <tr>
                        <td><label>Email</label></td>
                        <td><input type="text" id="useremail" name="useremail" style="width:15rem;" placeholder="Enter the email attribute name received from OAuth Server" value="<?php if (get_option('oc_useremail')) echo esc_attr(get_option('oc_useremail')); ?>" />
                    </tr>
                    <tr>
                        <td><label>First Name</label></td>
                        <td><input type="text" id="firstname" name="firstname" style="width:15rem;" placeholder="Enter the First Name attribute name received from OAuth Server" value="<?php if (get_option('oc_firstname')) echo esc_attr(get_option('oc_firstname')); ?>" />
                    </tr>
                    <tr>
                        <td><label>Last Name</label></td>
                        <td><input type="text" id="lastname" name="lastname" style="width:15rem;" placeholder="Enter the Last Name attribute name received from OAuth Server" value="<?php if (get_option('oc_lastname')) echo esc_attr(get_option('oc_lastname')); ?>" />
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 8rem; padding-top:1rem;" ><input class="buttons_style" type="submit" id="clientmapping" value="Save Attribute Mapping" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

    <script>
        function getattributes() {
            let testWindow = window.open('<?php echo site_url(); ?>' + '/?wpoauthcsso=authorization&getattributes=true', "Test Attribute Configuration", "width=600, height=600");
        }
    </script>
<?php
}

function oauth_help() {
?>
	<div class="card">
		<div>
        <h2>Coming soon.</h2>
		</div>
	</div>
<?php
}

function oauthclient_howto() {
	$params = "?wpoauthcsso=authorization";
	$signin_url = site_url() . $params;
	$signin_url_wpadmin = site_url('/wp-admin/') . $params;
	$signout_url = wp_logout_url('/');
?>
	<div class="card">
		<h2>URL to be used for sign-in (1)</h2>
		<ul><li><a href="<?php echo $signin_url; ?>"><?php echo $signin_url; ?></a></li></ul>
		<h2>URL to be used for sign-in (2)</h2>
		<ul><li><a href="<?php echo $signin_url_wpadmin; ?>"><?php echo $signin_url_wpadmin; ?></a></li></ul>
		<h2>URL to be used for sign-out</h2>
		<ul><li><a href="<?php echo $signout_url; ?>"><?php echo $signout_url; ?></a></li></ul>
	</div>
<?php
}
