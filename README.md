# WP OAuth Client SSO

This is the "WP OAuth Client SSO" WordPress plugin.

## With the help of ...

This is a fork of the "WordPress OAuth client SSO( OAuth 2.0 SSO)" by steve06

Its source code is located at:
- Trac: https://plugins.trac.wordpress.org/browser/oauth-client-for-user-authentication/
- SVN: https://plugins.svn.wordpress.org/oauth-client-for-user-authentication/

## Why?

I needed some changes and addition to the plugin that I've found so I decided to do it myself.

