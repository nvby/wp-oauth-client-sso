<?php // page can't get direct access

class OAuth_Client_Authenticate {
	public static function authorization_endpoint($auth_code_url, $client_id, $scope, $redirect_uri) {
		$state = self::genrate_state(); // TODO: MUST use generated "state" and verify later.
		$authorizationUrl = $auth_code_url .
			"?client_id=" . $client_id .
			"&scope=" . $scope .
			"&redirect_uri=" . $redirect_uri .
			"&response_type=code" .
			"&state=" . $state;
		if (session_id() == '' || !isset($session)) session_start();
		header('Location: ' . $authorizationUrl);
		exit;
	}

// Ref: https://stackoverflow.com/questions/54578397/ set client_secret ONLY if necessary
	public static function call_to_token_endpoint($token_endpoint_url, $redirect_uri, $client_id, $client_secret, $code, $grant_type) {
		$body = array(
			'grant_type'    => $grant_type,
			'code'          => $code,
			'client_id'     => $client_id,
			'redirect_uri'  => $redirect_uri,
		);
		if ($client_secret) $body['client_secret'] = $client_secret;
		$headers = array(
		   'Accept'  => 'application/json',
		   'charset'       => 'UTF - 8',
		   'Content-Type' => 'application/x-www-form-urlencoded',
		);
		if ($client_secret) $headers['Authorization'] = 'Basic ' . base64_encode($client_id . ':' . $client_secret);

		$response   = wp_remote_post($token_endpoint_url, array (
			 'method'      => 'POST',
			 'timeout'     => 45,
			 'redirection' => 5,
			 'httpversion' => '1.0',
			 'blocking'    => true,
			 'headers'     => $headers,
			 'body'        => $body,
			 'cookies'     => array(),
			 'sslverify'   => false
		));

		$response =  $response['body'];

		$response = json_decode($response, true);
		$access_token = $response['access_token'];

		return $access_token;
	}

	public static function call_to_user_info_endpoint($resourceownerdetailsurl, $access_token) {
		$headers = array();
		$headers['Authorization'] = 'Bearer ' . $access_token;

		$response   = wp_remote_post($resourceownerdetailsurl, array(
			 'method'      => 'GET',
			 'timeout'     => 45,
			 'redirection' => 5,
			 'httpversion' => '1.0',
			 'blocking'    => true,
			 'headers'     => $headers,
			 'cookies'     => array(),
			 'sslverify'   => false
		));
		$response =  $response['body'];

		$content = json_decode($response, true);
		if (isset($content["error_description"])) {
			exit($content["error_description"]);
		} else if (isset($content["error"])) {
			exit($content["error"]);
		}
		return $content;
	}

	public static function genrate_state() {
		return base_convert(random_int(62193781, 2176782335), 10, 36);
	}
}
