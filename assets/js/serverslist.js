let serverslist = [
    "select server",
    "AWS Cognito",
    'ADFS',
    'Amazon',
    'Azure AD',
    'Azure AD 2.0',
    'Azure B2C',
    'Bitrix24',
    'Clever',
    'Discord',
    'Google',
    'GitHub',
    'GitLab',
    'Invision Community',
    'Keycloak',
    'LinkedIn',
    'Office 365',
    'Okta',
    'OneLogin',
    'OpenAM',
    'PayPal',
    'Ping Identity',
    'Salesforce',
    'Slack',
    'WSO2 Identity Server',
    'WHMCS',
    'Zendesk',
    'Custom OAuth',
    'Custom OpenID'
];

let AWS_Cognito = {
    scope: "openid",
    authorization_endpoint: "https://<cognito-app-domain>/oauth2/authorize",
    token_endpoint: "https://<cognito-app-domain>/oauth2/token",
    userinfo_endpoint: "https://<cognito-app-domain>/oauth2/userInfo",
};

let ADFS = {
    scope: "openid",
    authorization_endpoint: "https://{yourADFSDomain}/adfs/oauth2/authorize/",
    token_endpoint: "https://{yourADFSDomain}/adfs/oauth2/token/",
};

let Amazon = {
    scope: "profile",
    authorization_endpoint: "https://www.amazon.com/ap/oa",
    token_endpoint: "https://api.amazon.com/auth/o2/token",
    userinfo_endpoint: "https://api.amazon.com/user/profile",
};

let Azure_AD = {
    scope: "openid",
    authorization_endpoint: "https://login.microsoftonline.com/[tenant-id]/oauth2/authorize",
    token_endpoint: "https://login.microsoftonline.com/[tenant]/oauth2/token",
};

let Azure_AD_2_0 = {
    scope: "openid",
    authorization_endpoint: "https://login.microsoftonline.com/{tenant}/oauth2/v2.0/authorize",
    token_endpoint: "https://login.microsoftonline.com/{tenant}/oauth2/v2.0/token",
};

let Azure_B2C = {
    scope: "openid",
    authorization_endpoint: "https://{tenant}.b2clogin.com/{tenant}.onmicrosoft.com/{policy}/oauth2/v2.0/authorize",
    token_endpoint: "https://{tenant}.b2clogin.com/{tenant}.onmicrosoft.com/{policy}/oauth2/v2.0/token",
}

let Bitrix24 = {
    scope: "user",
    authorization_endpoint: "https://{your-id}.bitrix24.com/oauth/authorize",
    token_endpoint: "https://{your-id}.bitrix24.com/oauth/token",
    userinfo_endpoint: "https://{your-id}.bitrix24.com/rest/user.current.json?auth=",
}

let Clever = {
    scope: "read:students read:teachers read:user_id",
    authorization_endpoint: "https://clever.com/oauth/authorize",
    token_endpoint: "https://clever.com/oauth/tokens",
    userinfo_endpoint: "https://api.clever.com/v1.1/me",
}

let Discord = {
    scope: "identify email guilds",
    authorization_endpoint: "https://discordapp.com/api/oauth2/authorize",
    token_endpoint: "https://discordapp.com/api/oauth2/token",
    userinfo_endpoint: "https://discordapp.com/api/users/@me",
}

let Google = {
    scope: "profile email",
    authorization_endpoint: "https://accounts.google.com/o/oauth2/auth",
    token_endpoint: "https://www.googleapis.com/oauth2/v4/token",
    userinfo_endpoint: "https://www.googleapis.com/oauth2/v1/userinfo",
}

let GitHub = {
    scope: "user repo",
    authorization_endpoint: "https://github.com/login/oauth/authorize",
    token_endpoint: "https://github.com/login/oauth/access_token",
    userinfo_endpoint: "https://api.github.com/user",
}

let GitLab = {
    scope: "read_user",
    authorization_endpoint: "https://gitlab.com/oauth/authorize",
    token_endpoint: "http://gitlab.com/oauth/token",
    userinfo_endpoint: "https://gitlab.com/api/v4/user",
}

let Invision_Community = {
    scope: "Email",
    authorization_endpoint: "https://{invision-community-domain}/oauth/authorize/",
    token_endpoint: "https://{invision-community-domain}/oauth/token/",
    userinfo_endpoint: "https://{invision-community-domain}/oauth/me",
}

let Keycloak = {
    scope: "openid",
    authorization_endpoint: "{your-domain}/auth/realms/{realm}/protocol/openid-connect/auth",
    token_endpoint: "{your-domain}/auth/realms/{realm}/protocol/openid-connect/token",
}

let LinkedIn = {
    scope: "r_basicprofile",
    authorization_endpoint: "https://www.linkedin.com/oauth/v2/authorization",
    token_endpoint: "https://www.linkedin.com/oauth/v2/accessToken",
    userinfo_endpoint: "https://api.linkedin.com/v2/me",
}

let Office_365 = {
    scope: "openid",
    authorization_endpoint: "https://login.windows.net/common/oauth2/authorize",
    token_endpoint: "https://login.windows.net/common/oauth2/token",
}

let Okta = {
    scope: "openid",
    authorization_endpoint: "https://{yourOktaDomain}.com/oauth2/default/v1/authorize",
    token_endpoint: "https://{yourOktaDomain}.com/oauth2/default/v1/token",
}

let OneLogin = {
    scope: "openid",
    authorization_endpoint: "https://<site-url>.onelogin.com/oidc/auth",
    token_endpoint: "https://<site-url>.onelogin.com/oidc/token",
}

let OpenAM = {
    scope: "Write",
    authorization_endpoint: "https://openam.example.com:8443/openam/oauth2/realms/root/authorize",
    token_endpoint: "https://openam.example.com:8443/openam/oauth2/realms/root/realms/customers/access_token",

}

let PayPal = {
    scope: "openid",
    authorization_endpoint: "https://www.paypal.com/signin/authorize",
    token_endpoint: "https://api.paypal.com/v1/oauth2/token",
}

let Ping_Identity = {
    scope: "edit",
    authorization_endpoint: "https://localhost:9031/as/authorization.oauth2",
    token_endpoint: "https://localhost:9031/as/token.oauth2",

}

let Salesforce = {
    scope: "email",
    authorization_endpoint: "https://login.salesforce.com/services/oauth2/authorize",
    token_endpoint: "https://login.salesforce.com/services/oauth2/token",
    userinfo_endpoint: "https://login.salesforce.com/services/oauth2/userinfo",
}

let Slack = {
    scope: "users.profile:read",
    authorization_endpoint: "https://slack.com/oauth/authorize",
    token_endpoint: "https://slack.com/api/oauth.access",
    userinfo_endpoint: "https://slack.com/api/users.profile.get",
}

let WSO2_Identity_Server = {
    scope: "openid",
    authorization_endpoint: "https://<wso2-app-domain>/wso2/oauth2/authorize",
    token_endpoint: "https://<wso2-app-domain>/wso2/oauth2/token",
    userinfo_endpoint: "https://<wso2-app-domain>/wso2/oauth2/userinfo",
}

let WHMCS = {
    scope: "openid profile email",
    authorization_endpoint: "https://{yourWHMCSdomain}/oauth/authorize.php",
    token_endpoint: "https://{yourWHMCSdomain}/oauth/token.php",
    userinfo_endpoint: "https://{yourWHMCSdomain}/oauth/userinfo.php?access_token=",
}

let Zendesk = {
    scope: "read",
    authorization_endpoint: "https://{subdomain}.zendesk.com/oauth/authorizations/new",
    token_endpoint: "https://{subdomain}.zendesk.com/oauth/tokens",
    userinfo_endpoint: "https://{subdomain}.zendesk.com/api/v2/users",
}

let Custom_OAuth = {
    scope: "",
    authorization_endpoint: "",
    token_endpoint: "",
    userinfo_endpoint: "",
}

let Custom_OpenID = {
    scope: "",
    authorization_endpoint: "",
    token_endpoint: "",
}

function getlistvalues(sname) {
    return eval(sname);
}
