let servers = serverslist;

let select = document.createElement("select");

select.name = "oauthservers";
select.id = "oauthserver";

servers.forEach(element => {
    var option = document.createElement("option");
    option.value = element.split(" ").join("_");
    option.value = option.value.split(".").join("_");
    option.text = element;
    select.appendChild(option);
    console.log(option.value);
});

document.getElementById("oc_oauthservernames").appendChild(select);

jQuery('#oauthserver').change(function (e) {
    let serverdetails = getlistvalues(e.target.value);
    
    document.getElementById("client_scope").value = serverdetails.scope;
    document.getElementById("client_authorization").value = serverdetails.authorization_endpoint;
    document.getElementById("client_token_endpoint").value = serverdetails.token_endpoint;
    
    if('undefined'!=typeof(serverdetails.userinfo_endpoint)){
        document.getElementById("oc_userinfo_field").style.display="";
        document.getElementById("client_userinfo_endpoint").value = serverdetails.userinfo_endpoint;
    }else{ 
        document.getElementById("oc_userinfo_field").style.display = "none";
        document.getElementById("client_userinfo_endpoint").value='';
    }
});