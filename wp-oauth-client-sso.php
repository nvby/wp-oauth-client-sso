<?php
/**
 * WP OAuth Client SSO
 *
 * @link                https://gitlab.com/nvby/wp-oauth-client-sso
 * @since               1.0.0
 * @package             WPOACSSO
 *
 * @wordpress-plugin
 * Plugin Name:         WP OAuth Client SSO
 * Plugin URI:          https://gitlab.com/nvby/wp-oauth-client-sso
 * Description:         Login and authenticate WordPress users using OAuth server credentials
 * Version:             2.3.3
 * Author:              Neven Boyanov
 * Author URI:          https://boyanov.org
 */

require_once 'wp-oauth-client-sso-layout.php';
require_once 'oauth-client-authenticate.php';
require_once (ABSPATH . 'wp-admin/includes/plugin.php');

class WP_OAuth_Client_SSO {
	protected static $instance = NULL;

	public static function getInstance() {
		if (!self::$instance) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	function __construct() {
		add_action('admin_menu', array($this, 'addMenuPage'));
		add_action('init', array($this, 'save_oauthclient_config'));
	}

	function addMenuPage() {
		add_menu_page('oauthclient', 'WP OAuth SSO', 'manage_options', 'WP OAuth Client SSO', 'oc_oauthclient_layout');
	}

	function save_oauthclient_config() {
		if (isset($_GET['wpoauthcsso']))	// NOTE: replace oauthclientsso with wpoauthcsso
			if ('authorization' == $_GET['wpoauthcsso']) {
				if ($_GET['getattributes'] == true)
					setcookie("getattributes", true);
				else
					setcookie("getattributes", false);
				$auth_code_url = get_option('oc_client_authorization');
				$client_id = get_option('oc_clientid');
				$scope = get_option('oc_clientscope');
				$redirect_uri = home_url('/wp-admin/');		// TODO: Put this value in an editable option.
				OAuth_Client_Authenticate::authorization_endpoint($auth_code_url, $client_id, $scope, $redirect_uri);
			}
		// NOTE: 'code' should be combined with another parameter, otherwise every link that contains 'code' will trigger this.
		// TODO: Change this, add another parameter to the request (GET) to identify that it is a OAuth request.
		if (isset($_GET['code'])) {
			$token_endpoint_url = get_option('oc_client_token_endpoint');
			$redirect_uri = home_url('/wp-admin/');			// TODO: Put this value in an editable option.
			$oauth_access_token = OAuth_Client_Authenticate::call_to_token_endpoint(
				$token_endpoint_url, $redirect_uri,
				get_option('oc_clientid'),
				get_option('oc_clientsecret'),
				$_GET['code'], 'authorization_code');
			if (isset($oauth_access_token)) {
				$userinfo_endpoint_url = get_option('oc_client_userinfo_endpoint');
				$user_info = OAuth_Client_Authenticate::call_to_user_info_endpoint($userinfo_endpoint_url, $oauth_access_token);

				if (isset($user_info) && $user_info && $_COOKIE['getattributes']) {
					echo '<pre>' . print_r($user_info, true) . '</pre>';
					unset($_COOKIE['getattributes']);
					exit;
				} else if (isset($user_info) && $user_info) {
					self::user_signin($user_info);
				}
			}
		}

		if (isset($_POST['action'])) {
			if ($_POST['action'] == 'oauthconfig') {
				if (isset($_POST['OAuthConfig_nonce']) && !empty($_POST['OAuthConfig_nonce'])) {
					update_option('oc_appname', isset($_POST['app_name']) ? sanitize_text_field($_POST['app_name']) : '');
					update_option('oc_clientid', isset($_POST['client_id']) ? sanitize_text_field($_POST['client_id']) : '');
					update_option('oc_clientsecret', isset($_POST['client_secret']) ? sanitize_text_field($_POST['client_secret']) : '');
					update_option('oc_clientscope', isset($_POST['client_scope']) ? $_POST['client_scope'] : '');
					update_option('oc_client_authorization', isset($_POST['client_authorization']) ? sanitize_text_field($_POST['client_authorization']) : '');
					update_option('oc_client_token_endpoint', isset($_POST['client_token_endpoint']) ? sanitize_text_field($_POST['client_token_endpoint']) : '');
					update_option('oc_client_userinfo_endpoint', isset($_POST['client_userinfo_endpoint']) ? sanitize_text_field($_POST['client_userinfo_endpoint']) : '');
				}
			} else if ($_POST['action'] == 'attributemapping') {
				if (isset($_POST['attributemapping_nonce']) && !empty($_POST['attributemapping_nonce'])) {
					update_option('oc_username', isset($_POST['username']) ? sanitize_text_field($_POST['username']) : '');
					update_option('oc_useremail', isset($_POST['useremail']) ? sanitize_text_field($_POST['useremail']) : '');
					update_option('oc_firstname', isset($_POST['firstname']) ? sanitize_text_field($_POST['firstname']) : '');
					update_option('oc_lastname', isset($_POST['lastname']) ? sanitize_text_field($_POST['lastname']) : '');
				}
			}
		}
	}

	function user_signin($userinfo) {
		// Ref: https://wordpress.stackexchange.com/questions/53503/
		$user = get_user_by('email', $userinfo['email'] );
		$user_id = $user->ID;

		$user_info = array();
		$user_info['first_name'] = $userinfo[get_option('oc_firstname')];
		$user_info['last_name'] = $userinfo[get_option('oc_lasttname')];

		if ($user_id) {
			// Update WP User with received info.
			$user_info['ID'] = $user_id;
			wp_update_user($user_info);
		} else {
			// Create new WP User
			// NOTE: Add this info ONLY for the new users.
			$user_info['user_login'] = $userinfo[get_option('oc_username')];
			$user_info['user_email'] = $userinfo[get_option('oc_useremail')];
			$user_info['user_pass'] = wp_generate_password(20);
			wp_insert_user($user_info);
		}

		// clean_user_cache($user); // Optional
		// wp_clear_auth_cookie();	// Optional: Removes all of the cookies associated with authentication.
		wp_set_current_user($user_id);
		wp_set_auth_cookie($user_id);
		do_action('wp_login', $user->user_login, $user);
		// update_user_caches($user); // Optional
		wp_safe_redirect(home_url());
		exit;
	}
}

$OAuth_Client = WP_OAuth_Client_SSO::getInstance();
